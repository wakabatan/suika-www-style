function WizardView (c) {
  if (c) {
    this.container = c;
    this.events = [];
    this.currentPage = -1;
    this._init ();
  }
} // WizardView

WizardView.prototype.enable = function () {
  this.deleteClass (this.container, 'wizardview-disabled');
  this.addClass (this.container, 'wizardview-enabled');

  var ev = new WizardView.Event ('enabled');
  this.dispatchEvent (ev);    
}; // enable

WizardView.prototype.disable = function () {
  this.deleteClass (this.container, 'wizardview-enabled');
  this.addClass (this.container, 'wizardview-disabled');

  var ev = new WizardView.Event ('disabled');
  this.dispatchEvent (ev);    
}; // disable

WizardView.prototype.getDisabled = function () {
  return !this.hasClass (this.container, 'wizardview-enabled');
}; // getDisabled

WizardView.prototype._init = function () {
  this._updateOutlineInfo ();
  this._initPanel ();
  this.addClass (this.container, 'wizardview');
  this.showFirstPage ();
}; // _init

WizardView.prototype._initPanel = function () {
  var outline = this._createOutlineBox ();
  this.outlineBox = outline;
  this._insertOutlineBox (outline);

  var controller = this._createController ();
  this._insertController (controller);
}; // _initPanel

WizardView.prototype.updateOutline = function () {
  this._updateOutlineInfo ();
  var newOutline = this._createOutlineBox ();
  var oldOutline = this.outlineBox;
  this.outlineBox = newOutline;
  this._insertOutlineBox (newOutline, oldOutline);
}; // updateOutline

WizardView.prototype._updateOutlineInfo = function () {
  this.pages = [];
  this.__updateOutlineInfo (this.container, []);
}; // _updateOutlineInfo

WizardView.prototype.__updateOutlineInfo = function (el, chain) {
  var hasPage;
  var elC = el.childNodes;
  var elCL = elC.length;
  for (var i = 0; i < elCL; i++) {
    var child = elC[i];
    if (child.nodeType == 1) {
      if (this.isWizardContainerOrPage (child)) {
        var newChain = chain.slice (0, chain.length);
        newChain.push (child);
        if (this.__updateOutlineInfo (child, newChain)) {
          hasPage = true;
        } else {
          this.pages.push (newChain);
          hasPage = true;
        }
      } else if (this.isTitle (child)) {
        if (chain.length) {
          chain[chain.length - 1].wizardTitle = this.getTitleText (child);
        }
      } else {
        if (this.__updateOutlineInfo (child, chain)) {
          hasPage = true;
        }
      }
    }
  }
  return hasPage;
}; // _updateOutlineInfo

WizardView.prototype.addEventListener = function (eventType, handler, phase) {
  if (!this.events[eventType]) this.events[eventType] = [];
  this.events[eventType].push (handler);
}; // addEventListener

WizardView.prototype.dispatchEvent = function (ev) {
  var eventType = ev.type;
  if (!this.events[eventType]) return true;
  ev.target = this;
  ev.currentTarget = this;
  var handlers = this.events[eventType];
  for (var i = 0; i < handlers.length; i++) {
    handlers[i] (ev);
  }
  return !ev.defaultPrevented;
}; // dispatchEvent

WizardView.prototype.createButton = function () {
  var el;

  try {
    el = document.createElement ('button');
    el.setAttribute ('type', 'button');
  } catch (e) {
    el = document.createElement ('<button type=button>');
  }
  
  return el;
}; // createButton

WizardView.prototype.createSubmitButton = function () {
  var el;

  try {
    el = document.createElement ('button');
    el.setAttribute('type', 'submit');
  } catch (e) {
    el = document.createElement ('<button type=submit>');
  }
  
  return el;
}; // createSubmitButton

WizardView.prototype.getTextContent = function (el) {
  var r = '';
  var elC = el.childNodes;
  var elCL = elC.length;
  for (var i = 0; i < elCL; i++) {
    var child = elC[i];
    if (child.nodeType == 3 || child.nodeType == 4) {
      r += child.data;
    } else if (child.nodeType == 1) {
      r += this.getTextContent (child);
    }
  }
  return r;
}; // getTextContent

WizardView.prototype.normalizeClassName = function (c) {
  if (document.compatMode == 'BackCompat') {
    return c.toLowerCase ();
  } else {
    return c;
  }
}; // c

WizardView.prototype.addClass = function (el, className) {
  el.className += ' ' + className;
}; // addClass

WizardView.prototype.deleteClass = function (el, className) {
  var c = this.normalizeClassName (el.className).split (/\s+/);
  className = this.normalizeClassName (className);
  var C = [];
  for (var i = 0; i < c.length; i++) {
    if (c[i] != className) {
      C.push (c[i]);
    }
  }
  el.className = C.join (' ');
}; // deleteClass

WizardView.prototype.hasClass = function (el, className) { 
  var c = this.normalizeClassName (el.className).split (/\s+/);
  className = this.normalizeClassName (className);
  for (var i = 0; i < c.length; i++) {
    if (c[i] == className) {
      return true;
    }
  }
  return false;
}; // hasClass

WizardView.prototype.isActivePage = function (n) {
  var page = this.pages[n];
  for (var i = 0; i < page.length; i++) {
    if (page[i].getAttribute ('hidden')) {
      return false;
    }
  }
  return true;
}; // isActivePage

WizardView.prototype.getNextPageOf = function (n) {
  A: while (++n < this.pages.length && n >= 0) {
    if (this.isActivePage(n)) {
      return n;
    }
  }
  return null;
}; // getNextPageOf

WizardView.prototype.getPreviousPageOf = function (n) {
  A: while (--n >= 0 && n < this.pages.length) {
    if (this.isActivePage(n)) {
      return n;
    }
  }
  return null;
}; // getPreviousPageOf

WizardView.prototype.showActivePage = function (n) {
  if (this.isActivePage (n)) {
    return this.showPage (n);
  } else {
    return this.showPage (this.getNextPageOf (n) || this.getPreviousPageOf (n));
  }
}; // showActivePage

WizardView.prototype.showPage = function (n) {
  if (n == null) return;
  if (n < 0 || n >= this.pages.length) return;
  for (var i = 0; i < this.pages.length; i++) {
    var page = this.pages[i];
    for (var j = 0; j < page.length; j++) {
      this.deleteClass (page[j], 'wizardview-active');
      this.addClass (page[j], 'wizardview-inactive');
    }
  }
  var page = this.pages[n];
  for (var j = 0; j < page.length; j++) {
    this.deleteClass (page[j], 'wizardview-inactive');
    this.addClass (page[j], 'wizardview-active');
  }

  this.currentPage = n;
  this._markCurrentPageInOutlineBox (n);

  var ev = new WizardView.Event ('previouspagedisablednesschange');
  ev.disabled = this.getPreviousPageOf (n) == null;
  this.dispatchEvent (ev);    

  var ev = new WizardView.Event ('nextpagedisablednesschange');
  ev.disabled = this.getNextPageOf (n) == null;
  this.dispatchEvent (ev);    

  var ev = new WizardView.Event ('showpage');
  ev,pageNumber = n;
  this.dispatchEvent (ev);    
}; // showPage

WizardView.prototype.showFirstPage = function () {
  this.showPage (this.getNextPageOf (-1));
}; // showFirstPage

WizardView.prototype.showPreviousPage = function () {
  this.showPage (this.getPreviousPageOf (this.currentPage));
}; // showPreviousPage

WizardView.prototype.showNextPage = function () {
  this.showPage (this.getNextPageOf (this.currentPage));
}; // showNextPage

/* TODO: Hyperlink support (e.g. onhashchange) */

WizardView.prototype.showElement = function (el) {
  while (!this.isWizardContainerOrPage (el)) {
    el = el.parentNode;
  }
  if (!el) return;

  var pages = this.pages;
  for (var i = 0; i < pages.length; i++) {
    var page = pages[i];
    if (page[page.length - 1] == el) {
      return this.showPage (i);
    }
  }
}; // showElement



WizardView.Event = function (eventType) {
  this.type = eventType;
}; // Event

WizardView.Event.prototype.preventDefault = function () {
  this.defaultPrevented = true;
}; // preventDefault



/* You might want to override methods below for customizing how wizard
pages are organized and to control page flow. */

WizardView.prototype._createOutlineBox = function () {
  var list = document.createElement ('ol');
  for (var i = 0; i < this.pages.length; i++) {
    var page = this.pages[i];
    var li = document.createElement ('li');
    li.innerHTML = 'Page ' + (i + 1);
    if (page[page.length - 1].wizardTitle) {
      li.firstChild.data = page[page.length - 1].wizardTitle;
    }
    list.appendChild (li);
  }
  return list;
}; // _createOutlineBox

WizardView.prototype._markCurrentPageInOutlineBox = function (n) {
  var list = this.outlineBox;
  for (var i = 0; i < list.childNodes.length; i++) {
    var li = list.childNodes[i];
    if (i == n) {
      this.addClass (li, 'wizard-selected-page');
    } else {
      this.deleteClass (li, 'wizard-selected-page');
    }
  }
}; // _markCurrentPageInOutlineBox

WizardView.prototype._insertOutlineBox = function (newOutline, oldOutline) {
  if (oldOutline) {
    this.container.replaceChild (newOutline, oldOutline);
  } else {
    this.container.appendChild (newOutline);
  }
}; // _insertOutlineBox

WizardView.prototype._createController = function () {
  var self = this;

  var controller = document.createElement ('nav');

  var prevButton = this.createButton ();
  prevButton.innerHTML = 'Back';
  prevButton.onclick = function () { self.showPreviousPage () };
  this.addEventListener ('previouspagedisablednesschange', function (e) {
    prevButton.disabled = e.disabled;
  });
  controller.appendChild (prevButton);

  var nextButton = this.createButton ();
  nextButton.innerHTML = 'Next';
  nextButton.onclick = function () { self.showNextPage () };
  this.addEventListener ('nextpagedisablednesschange', function (e) {
    nextButton.disabled = e.disabled;
  });
  controller.appendChild (nextButton);

  var finButton = this.createButton ();
  finButton.innerHTML = 'Finish';
  finButton.onclick = function () { self.finish () };
  this.addEventListener ('finishdisablednesschange', function (e) {
    finButton.disabled = e.disabled;
  });
  controller.appendChild (finButton);

  var cancelButton = this.createButton ();
  cancelButton.innerHTML = 'Cancel';
  cancelButton.onclick = function () { self.cancel () };
  this.addEventListener ('canceldisablednesschange', function (e) {
    cancelButton.disabled = e.disabled;
  });
  controller.appendChild (cancelButton);
  
  return controller;
}; // _createController

WizardView.prototype._insertController = function (controller) {
  this.container.appendChild (controller);
}; // _insertController

WizardView.prototype.finish = function () {

}; // finish

WizardView.prototype.cancel = function () {

}; // cancel

WizardView.prototype.isWizardContainerOrPage = function (el) {
  var tagName = el.tagName.toLowerCase ();
  if (tagName == 'section' || 
      (tagName == 'div' && this.hasClass (el, 'section'))) {
    return true;
  } else {
    return false;
  }
}; // isWizardContainerOrPage

WizardView.prototype.isTitle = function (el) {
  if (el.tagName.match (/^[Hh][1-6]$/)) {
    return true;
  } else {
    return false;
  }
}; // isTitle

WizardView.prototype.getTitleText = function (el) {
  return el.title.length ? el.title : this.getTextContent (el);
}; // getTitleText

if (window.WizardViewOnLoad) window.WizardViewOnLoad ();

/* 

Usage:

  <script src="https://suika.suikawiki.org/www/style/ui/wizard.js" charset=utf-8></script>
  <script>
    window.onload = function () {
      var w = new WizardView (container);
      w.enable ();
    };
  </script>

Note: Don't try to use this script with a large container element.
This script scans all descendant nodes in the container element to
find wizard container, page, and title elements upon the
initializatioon and rewrites class="" attribute of all page elements
whenever the current page changes.

*/

/* ***** BEGIN LICENSE BLOCK *****
 * Copyright 2008 Wakaba <wakaba@suikawiki.org>.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the same terms as Perl itself.
 *
 * Alternatively, the contents of this file may be used 
 * under the following terms (the "MPL/GPL/LGPL"), 
 * in which case the provisions of the MPL/GPL/LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of the MPL/GPL/LGPL, and not to allow others to
 * use your version of this file under the terms of the Perl, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the MPL/GPL/LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the Perl or the MPL/GPL/LGPL.
 *
 * "MPL/GPL/LGPL":
 *
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <http://www.mozilla.org/MPL/>
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is WizardView code.
 *
 * The Initial Developer of the Original Code is Wakaba.
 * Portions created by the Initial Developer are Copyright (C) 2008
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Wakaba <wakaba@suikawiki.org>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the LGPL or the GPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
