function WidgetValues (c) {
  if (c) {
    this.container = c;
    this._init ();
  }
} // WidgetValues

WidgetValues.prototype._init = function () {
  var ta = this.container.getElementsByTagName ('textarea')[0];
  if (!ta) return;

  this.name = this.container.getAttribute ('data-widget-name') || ta.name || '';

  this.separatorPattern
      = new RegExp (this.container.getAttribute ('data-widget-separator')
                        || '\\s+');
  
  this.defaultValues = ta.defaultValue.length
    ? ta.defaultValue.split (this.separatorPattern) : [];
  this.initialValues = ta.value.length
    ? ta.value.split (this.separatorPattern) : [];

  this.clear ();
  this.appendValues (this.initialValues);
}; // _init

WidgetValues.prototype._clear = function () {
  this.firstField = null;
  this.lastField = null;
  this.container.innerHTML = '';
}; // _clear

WidgetValues.prototype.appendValues = function (values) {
  var self = this;
  for (var i = 0; i < values.length; i++) {
    var field = this.createField (values[i]);
    field.wvRemove = function () { self._removeField (this) };
    field.wvMoveUp = function () { self._moveUpField (this) };
    field.wvMoveDown = function () { self._moveDownField (this) };
    var parent = this.lastField ? this.lastField.parentNode : null;
    parent = parent ? parent : this.fieldsParent;
    parent.insertBefore (field, this.lastField ? this.lastField.nextSibling : null);
    this.lastField = field;
    this.firstField = this.firstField || field;
  }
}; // appendValues

WidgetValues.prototype.reset = function () {
  this.clear ();
  this.appendValues (this.defaultValues);
}; // reset

WidgetValues.prototype._removeField = function (field) {
  if (this.firstField == field) {
    this.firstField = this._getNextField (field);
  }
  if (this.lastField == field) {
    this.lastField = this._getPreviousField (field);
  }
  field.parentNode.removeChild (field);
}; // _removeField

WidgetValues.prototype._moveUpField = function (field) {
  this._swapFields (field, this._getPreviousField (field));
}; // _moveUpField

WidgetValues.prototype._moveDownField = function (field) {
  this._swapFields (field, this._getNextField (field));
}; // _moveDownField

WidgetValues.prototype._swapFields = function (f1, f2) {
  if (!f1 || !f2) return;
  var ph = document.createElement (f1.tagName || 'x');
  f1.parentNode.replaceChild (ph, f1);
  f2.parentNode.replaceChild (f1, f2);
  ph.parentNode.replaceChild (f2, ph);
}; // _swapFields

WidgetValues.prototype._getNextField = function (field) {
  var n = field.nextSibling;
  while (n != null) {
    if (this.isField (n)) {
      return n;
    }
    n = n.nextSibling;
  }
  return null;
}; // _getNextField

WidgetValues.prototype._getPreviousField = function (field) {
  var n = field.previousSibling;
  while (n != null) {
    if (this.isField (n)) {
      return n;
    }
    n = n.previousSibling;
  }
  return null;
}; // _getPreviousField

WidgetValues.prototype.createButton = function () {
  var el;

  try {
    el = document.createElement ('button');
    el.setAttribute('type', 'button');
  } catch (e) {
    el = document.createElement ('<button type=button>');
  }

  return el;
}; // createButton

WidgetValues.prototype.isField = function (n) {
  return n.wvIsField;
}; // isField




WidgetValues.prototype.createField = function (value) {
  var field = document.createElement ('div');
  field.wvIsField = true;

  var input = document.createElement ('input');
  input.name = this.name;
  input.value = value;
  field.appendChild (input);

  var removeButton = this.createButton ();
  removeButton.innerHTML = 'Remove';
  removeButton.onclick = function () { field.wvRemove () };
  field.appendChild (removeButton);

  var moveUpButton = this.createButton ();
  moveUpButton.innerHTML = 'Move up';
  moveUpButton.onclick = function () { field.wvMoveUp () };
  field.appendChild (moveUpButton);

  var moveDownButton = this.createButton ();
  moveDownButton.innerHTML = 'Move down';
  moveDownButton.onclick = function () { field.wvMoveDown () };
  field.appendChild (moveDownButton);

  return field;
}; // createField

WidgetValues.prototype.clear = function () {
  var self = this;

  this._clear ();

  var fields = document.createElement ('div');
  this.container.appendChild (fields);
  this.fieldsParent = fields;

  var addButton = this.createButton ();
  addButton.innerHTML = 'Add';
  addButton.onclick = function () { self.appendValues (['']) };
  this.container.appendChild (addButton);

  var resetButton = this.createButton ();
  resetButton.innerHTML = 'Reset';
  resetButton.onclick = function () { self.reset () };
  this.container.appendChild (resetButton);
}; // clear

if (window.WidgetValuesOnLoad) WidgetValuesOnLoad ();

/*

Usage:

  <script src="http://suika.fam.cx/www/style/ui/widget-values.js.u8" charset=utf-8></script>
  <script>
    window.onload = function () {
      new WidgetValues (document.getElementById ('c'));
    };
  </script>
  
  <div id=c data-widget-name=test-value data-widget-separator="[\r\n]+">
  <textarea name=test-values>
  default
  values
  separated
  by
  newlines
  </textarea>
  </div>

The |WidgetValues| constructor takes an argument, which represents the
container element.  The container element MUST contain a |textarea|
element, which represents the default values.  The content of the
container element is replaced by indivisual input fields (called
"fields" in this script) and additional contents such as an "add"
button.

The |data-widget-name| attribute of the container element represents
the name of the control, used by the form controls in the fields as
the |name| attribute value.  This attribute is REQURIED.

The |data-widget-separator| attribute of the container element
represents a JavaScript regular expression that matches to substrings
used to separate values in the |textarea| element.  This attribute is
optional; the default separator is /\s+/ (i.e. spaces).

You can customize how fields and additional contents are organized in
the container element by creating a subclass and inheriting
|createField| and |clear| methods, like the example below:

  // <http://suika.fam.cx/~wakaba/-temp/test/misc/jswidgets/values/values-2.html>
  
  function MyNumbersWidget (c) {
    WidgetValues.apply (this, [c]);
  }
  
  MyNumbersWidget.prototype = new WidgetValues;
  
  MyNumbersWidget.prototype.createField = function (value) {
    var field = document.createElement ('span');
    field.wvIsField = true;
    
    var input = document.createElement ('input');
    input.setAttribute ('type', 'number');
    input.setAttribute ('min', 1);
    input.setAttribute ('max', 10);
    field.appendChild (input);
    
    return field;
  };
  
  MyNumbersWidget.prototype.clear = function () {
    var self = this;
    
    this._clear ();
    
    var fields = document.createElement ('span');
    this.container.appendChild (fields);
    this.fieldsParent = fields;
    
    var addButton = this.createButton ();
    addButton.innerHTML = '+';
    addButton.onclick = function () { self.appendValues ([5]) };
    this.container.appendChild (addButton);
  };

Latest version of this script is available at
<http://suika.fam.cx/www/style/ui/widget-values.js.u8>.  Old versions
of this script are available from
<http://suika.fam.cx/www/style/ui/widget-values.js.u8,cvslog>.

*/

/* ***** BEGIN LICENSE BLOCK *****
 * Copyright 2008 Wakaba <w@suika.fam.cx>.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the same terms as Perl itself.
 *
 * Alternatively, the contents of this file may be used 
 * under the following terms (the "MPL/GPL/LGPL"), 
 * in which case the provisions of the MPL/GPL/LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of the MPL/GPL/LGPL, and not to allow others to
 * use your version of this file under the terms of the Perl, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the MPL/GPL/LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the Perl or the MPL/GPL/LGPL.
 *
 * "MPL/GPL/LGPL":
 *
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <http://www.mozilla.org/MPL/>
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is WidgetValues code.
 *
 * The Initial Developer of the Original Code is Wakaba.
 * Portions created by the Initial Developer are Copyright (C) 2008
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Wakaba <w@suika.fam.cx>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the LGPL or the GPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
